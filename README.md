# Debian Junior Live System

Debian Junior Live System

This repository is based on the Debian Live System:

https://live-team.pages.debian.net/live-manual/html/live-manual/managing-a-configuration.en.html

The auto script has been created via:

```
cp /usr/share/doc/live-build/examples/auto/* auto/
```

## Build

Build your own Debian Jr. Live System:

 * apt-get install live-build
 * git clone https://salsa.debian.org/debian-jr-team/debian-junior-live-system.git
 * cd debian-junior-live-system/
 * lb config
 * sudo lb build

Run ISO with qemu:

 * kvm -m 2G -cdrom live-image-amd64.hybrid.iso

## Contact

Development and support:

 * Mailing list: The primary contact for the project is the mailing list at https://lists.debian.org/debian-jr/
 * XMPP/Jabber chat: xmpp:debian-jr@conference.debian.org?join

