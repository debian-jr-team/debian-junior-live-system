# Handbuch für Debian Junior Live System

Debian Junior ist ein freies Betriebssystem für Kinder. Das
Betriebssystem welches verwendet wird ist [Debian
GNU/Linux](https://www.debian.org).

## Fenstermanager
Als Fenstermanager wird IceWM verwendet. IceWN ist sehr
übersichtlich und hat wenige Funktionen. Außerdem ist es ähnlich
wie andere bekannte Betriebssystem aufgebaut und ermöglicht so
eine einfache und schnelle Einarbeitung für Personen, die schon
andere Betriebssystem kennen.

Unten links in der Ecke finden Sie das Menü. Mit einem Klick auf
die Schaltfläche "IceWM" öffnet sich das Menü und man findet
viele Anwendungsprogramme, Spiele und Werkzeuge.

Neben dem Menü finden sich weitere Schaltflächen:

 * Alle offenen Anwendungen minimieren
 * Alle offenen Fenster anzeigen
 * Virtuelle Desktops
 * Anwendungen in der Toolbar

## Programme

Wir haben eine Auswahl an Programme für Kinder bereitgestellt.
Debian GNU/Linux bietet weit mehr Programme an als auf diesem
System zur Verfügung gestellt wird.

 * thunar
 * mousepad
 * dino-im
 * vlc
 * claws-mail
 * epiphany-browser
 * gcompris-qt
 * ktuberling
 * tuxpaint
 * gamine
 * pysiogame
 * numptyphysics
 * stellarium
 * scratch
 * laby
 * frozen-bubble
 * brainparty
 * biniax2
 * lbreakout2
 * supertux
 * tuxpuck
 * jumpnbump
 * circuslinux
 * bumprace
 * hannah
 * supertuxkart
 * atomix
 * blobby
 * bloboats
 * blockout2
 * gnome-mahjongg
 * gnome-sudoku
 * gnome-nibbles
 * gtetrinet
 * gtkpool
 * hexxagon
 * ltris
 * tuxtype
 * tuxmath
 * scratch
 * kturtle
 * littlewizard
 * 2048-qt
 * gbrainy
 * ri-li
 * pingus
 * opencity

## Was Sie über Debian GNU/Linux wissen sollten

Das Betriebssystem besteht aus 100% freier Software. Es gibt
jedoch Geräte die nicht-freie Software benötigen. Dies sind
beispielweise Laptops mit WLAN Chips oder bestimmte Grafikkarten.
Um die Geräte ansprechen zu können, braucht man Firmware der Chip
Hersteller. Dies sind in vielen Fällen nicht als Freie Software
verfügbar.  

## Ich habe mich abgemeldet

Der Benutzer ist `user` und das Passwort `live`.
